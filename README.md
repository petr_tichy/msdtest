# MSDtest


This is a Ansible Playbook that should complete the following tasks required:

## Server with Main System Information

Required Tools: Ansible, Python

### Goal:

* Create a process for automatic deployment of a system backend application that shows system information
* Nginx server is installed too and is used as a reverse proxy in front of the application and provides SSL communication based on self-signed certificate

### Instructions:
1. Implement as much as possible
1. Instantiation of the host (e.g. creating the machine in a cloud) is NOT part of the task
1. Use latest Ansible to create roles and playbooks for the goal

### Implemented items:
1. Web server is installed on the machine
1. Server can be accessible from your local computer using hostname not just ip
1. Web page shows IP of the machine and whether numbers are odd or even
1. Web page shows current date
1. Web page shows current time
1. Web page shows usage of CPU of the machine
1. Service management is configured (start, stop, restart, status)
1. Tools like supervisord may be used for this

### Results:
1. All source codes in Github (or some other public git repository)
1. Readme.MD documentation how to use it
1. Found problems, not implemented features, etc. should be mentioned there



# Solution

The provided Ansible playbook install a Perl script Sysinfo.pl, which is run as a FastCGI backend, handling AJAX requests from the client. This is kept alive using [Supervisord](http://supervisord.org)

The NGINX server provides SSL and acts as FactCGI reverse proxy.

## Requirements

* Ansible >2
* The following Ansible Galaxy roles (see `requirements.yml`)
	* geerlingguy.ntp
	* geerlingguy.nginx
	* geerlingguy.pip
	* geerlingguy.supervisor

* A Debian or RedHat distribution with python installed

## Usage

Clone this repository

`git clone git@bitbucket.org:petr_tichy/msdtest.git`

Install requirements

`ansible-galaxy install -r requirements.yml`

Setup SSH with pubkey authentication and passwordless `sudo` to the host, editing `inventories/development/hosts` as appropriate

Run Ansible

`ansible-playbook -i inventories/development -D site.yml`

Point your browser to the installed site and enjoy the sysinfo app.

## Notes

The `development` environment contains a single host `localhost` that is assumed to run virtualized on the development machine. SSH connection is mapped to port 22322, and HTTP and HTTPS ports are forwarded to 8888 and 8443 respectively using

`ssh localhost -p22322 -L8888:localhost:80 -L8443:localhost:443`

The installed certificate is 90 days LetsEncrypt issued for https://localhost.example.spaceboy.cz (mapping to 127.0.0.1)

This playbook disables SELinux on RedHat hosts

## Testing

This playbook contains simple Serverspec test to be used with `ansible_spec` Ruby gem. Run the following on fully deployed host:

```
gem install ansible_spec
rake all
```

Currently these test check only for open HTTP, HTTPS and FastCGI ports.

## Found issues

The `Sys::Info` Perl module is pretty obsolete and unmaintained, and doesn't install using CPANminus (see https://rt.cpan.org/Public/Bug/Display.html?id=120898), so I had to install it using plain old CPAN shell.
(I've used the `Sys::Info` module before, and was hoping to reuse it here, but this create the above challenge of getting it installed). Finally abandoned the module altogether, as it doesn't install on recent CentOS

I'm not sure how to properly solve the circular dependency between NGINX service and SSL certificate. Specifying `notify: restart nginx` on the SSL certificate install before including the `geerlingguy.nginx` role leads to undefined handler. Including the `geerlingguy.nginx` role first makes the MGINX server fail due to missing SSL certificate. To solve this I created `nginx_handler` in the `webtier` role that reloads NGINX on SSL certificate install. IMHO there should be a nicer way to do this.
