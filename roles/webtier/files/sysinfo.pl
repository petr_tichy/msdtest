#!/usr/bin/perl

# SysInfo - Simple System Information
# written by Petr Tichy <petr.tichy@me.com>

use strict;
use warnings;

use HTML::Entities;

use CGI::Fast;


# html_escape
sub html_escape {
  my @result = map(encode_entities($_), @_);
  return(join('',@result));
}

sub get_datetime {
  return(`/bin/date -u -Iseconds`);
}

sub get_os_info {
  my $result;
  $result .= sprintf "Operating System is %s\n", `/bin/uname -a`;
  $result .= sprintf "Perl version is %vd\n", $^V;
  $result .= sprintf "%s\n", `/bin/uptime -p`;
  return($result);
}

sub get_cpu_info {
  return(`/usr/bin/lscpu`);
}

sub get_files_info {
  my $result;
  my $proc_file_nr = "/proc/sys/fs/file-nr";
  if( -e $proc_file_nr ) {
    open(my $proc_file_nr_fh, '<', $proc_file_nr);
    my ($file_nr_cur, $file_nr_free, $file_nr_max) = split(' ', <$proc_file_nr_fh>);
    close($proc_file_nr_fh);
    $file_nr_cur -= $file_nr_free;
    $result .= sprintf "Number of open files: %s\n", $file_nr_cur;
    $result .= sprintf "Max supported number of open files: %s\n", $file_nr_max;
    return($result);
  }
}

sub get_w_info {
  return(`/usr/bin/w`);
}

sub get_ps_info {
  return(`/bin/ps -N --ppid 2 flww`);
}

sub get_ip_info {
  return(`/sbin/ip addr show`);
}

# Split URI into name/value pairs
sub split_uri {
  my ($buffer, @pairs, $pair, $name, $value, %FORM);
  $buffer = shift;
  @pairs = split(/&/, $buffer);
  foreach $pair (@pairs)
  {
    ($name, $value) = split(/=/, $pair);
    $value =~ tr/+/ /;
    $value =~ s/%(..)/pack("C", hex($1))/eg;
    $FORM{$name} = $value;
  }
  return(%FORM);
}

# main
my (%FORM, $buffer);

my $dispatch_table = {
  get_datetime   => \&get_datetime,
  get_os_info    => \&get_os_info,
  get_cpu_info   => \&get_cpu_info,
  get_files_info => \&get_files_info,
  get_w_info     => \&get_w_info,
  get_ps_info    => \&get_ps_info,
  get_ip_info    => \&get_ip_info,
};

while (new CGI::Fast) {
  $ENV{'REQUEST_METHOD'} =~ tr/a-z/A-Z/;
  if ($ENV{'REQUEST_METHOD'} eq "GET" && ($buffer = $ENV{'QUERY_STRING'})) {
    %FORM = split_uri($buffer);
    if (exists $FORM{'action'}) {
      my $action = $FORM{'action'};
      exists $dispatch_table->{$action} or die "Invalid action";
      print "Content-type: text/plain\n\n";
      print html_escape($dispatch_table->{$action}->());
    }
  }
}
